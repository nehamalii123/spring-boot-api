package com.example.demospringboot;

import org.assertj.core. api.Assertions;
import org.junit.jupiter.api.Test;

public class RunTest {
    MyController myController = new MyController();
    @Test
    public void testHello() {
        String result = myController.hello();
        Assertions.assertThat(result).isEqualTo("Hello world Neha Here");

    }
}
